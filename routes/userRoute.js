const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/userController");

// Register a user
router.post("/register", (req, res) => {
  userController.registerUser(req.body)
    .then((success) => {
      if (success) {
        res.json({ success: true, message: "User registered successfully" });
      } else {
        res.json({ success: false, message: "Failed to register user" });
      }
    })
    .catch((error) => {
      console.error(error); // Log the error for debugging purposes
      res.status(500).json({ success: false, message: "Internal server error" });
    });
});

// Set user as an ADMIN
router.put("/:userId/setAdmin", auth.authenticateToken, auth.isAdmin, (req, res) => {
  const { userId } = req.params;
  userController.setAdminUser(userId)
    .then((success) => {
      if (success) {
        res.json({ success: true, message: "User is now an admin" });
      } else {
        res.json({ success: false, message: "Failed to set user as admin" });
      }
    })
    .catch((error) => {
      console.error(error); // Log the error for debugging purposes
      res.status(500).json({ success: false, message: "Internal server error" });
    });
});



// Route for User Authentication (ADMIN)
router.post("/login", (req, res) => {
  userController.loginUser(req.body).then(resultFromController => {
    if (resultFromController.access) {
      res.json({ success: true, access_token: resultFromController.access });
    } else {
      res.json({ success: false, message: "Authentication failed" });
    }
  });
});

// user authentication (NON-ADMIN)
router.post("/logInnonAdmin", (req, res) => {
  userController.logInnonAdminuser(req.body).then((resultFromController) => {
    if (resultFromController.success) {
      res.json({ success: true, access_token: resultFromController.access_token });
    } else {
      res.json({ success: false, message: resultFromController.message });
    }
  });
});


// Retrieve User Details (GET)
router.get("/details/:userId", auth.verify, (req, res) => {
  const userId = req.params.userId;

  // Retrieve user details
  userController.getProfile(userId)
    .then(user => {
      // Reassign the password to an empty string
      user.password = "";
      res.json(user);
    })
    .catch(error => {
      res.status(500).json({ error: "Failed to retrieve user details" });
    });
}); 


// checkouy=t route
router.post('/checkout', auth.verify, auth.isNonAdmin, userController.createOrder);




module.exports = router;
