const express = require("express");
const router = express.Router();
const auth = require("../auth");
const productController = require("../controllers/productController");

// Route for creating a product (ADMIN)
router.post("/", auth.verify, productController.addProduct);

// Route for retrieving all products 
router.get("/allProducts", productController.getAllProducts);

// Route for retrieving all active products 
router.get("/active", productController.getAllActiveProducts);

// Route for retrieving a single product 
router.get("/:id", productController.getProductById);

// Route for updating product information (ADMIN)
router.put("/:id", auth.verify, productController.updateProduct);

// Route for Activate Product (ADMIN)
router.put("/activate/:id", auth.authenticateToken, auth.isAdmin, productController.activateProduct);

// Route for Archive Product (ADMIN)
router.delete("/archive/:id", auth.verify, productController.archiveProduct);





module.exports = router;