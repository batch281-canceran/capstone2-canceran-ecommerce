const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI"


module.exports.isNonAdmin = (req, res, next) => {
  if (!req.user.isAdmin) {
    next();
  } else {
    return res.status(403).json({ message: 'Forbidden' });
  }
};


module.exports.verifyNonAdmin = (req, res, next) => {
  let token = req.headers.authorization;
  if (typeof token !== 'undefined') {
    token = token.slice(7, token.length);
    jwt.verify(token, secret, (err, decodedToken) => {
      if (err) {
        return res.status(401).json({ auth: 'failed' });
      } else {
        if (!decodedToken.isAdmin) { // Check if the user is not an admin
          req.user = decodedToken;
          next();
        } else {
          return res.status(403).json({ message: 'Forbidden' });
        }
      }
    });
  } else {
    return res.status(401).json({ auth: 'failed' });
  }
};
module.exports.authenticateToken = (req, res, next) => {
  let token = req.headers.authorization;
  if (typeof token !== 'undefined') {
    token = token.slice(7, token.length);
    jwt.verify(token, secret, (err, decodedToken) => {
      if (err) {
        return res.status(401).json({ auth: 'failed' });
      } else {
        req.user = decodedToken;
        next();
      }
    });
  } else {
    return res.status(401).json({ auth: 'failed' });
  }
};

module.exports.isAdmin = (req, res, next) => {
  if (req.user.isAdmin) {
    next();
  } else {
    return res.status(403).json({ message: 'Forbidden' });
  }
};




module.exports.createAccessToken = (user, isAdmin) => {
  const data = {
    id: user._id,
    email: user.email,
    isAdmin: isAdmin, // Use the provided isAdmin parameter instead of user.isAdmin
  };
  return jwt.sign(data, secret, {});
};

module.exports.verify = (req, res, next) => {
  let token = req.headers.authorization;
  if (typeof token !== 'undefined') {
    token = token.slice(7, token.length);
    return jwt.verify(token, secret, (err, decodedToken) => {
      if (err) {
        return res.status(401).json({ auth: 'failed' });
      } else {
        req.user = decodedToken;
        next();
      }
    });
  } else {
    return res.status(401).json({ auth: 'failed' });
  }
};

module.exports.decode = (token) => {
  if (typeof token !== 'undefined') {
    token = token.slice(7, token.length);
    const decodedToken = jwt.decode(token, { complete: true });
    if (decodedToken) {
      const { payload } = decodedToken;
      return payload;
    }
  }
  return null;
};

module.exports.secret = secret; // Expose the secret key for other modules to use
