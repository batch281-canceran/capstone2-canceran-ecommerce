const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const bodyParser = require('body-parser');

//routes
const userRoutes = require("./routes/userRoute");
const productRoutes = require("./routes/productRoute");
const auth = require("./auth.js");
//MAIN ENTRY
const app = express();

//Connect to our MongoDB
mongoose.connect("mongodb+srv://ayiejannielaus1:kyfcqC12gQRH6Tsq@wdc028-course-booking.ypsm2dj.mongodb.net/CAPSTONE2?retryWrites=true&w=majority",{
    useNewUrlParser: true,
    useUnifiedTopology: true
});

//Database connection 
let db = mongoose.connection;

// Event listen
db.on("error",console.error.bind(console,"MongoDB connection Error."));
db.once("open",() => console.log("No Errors, Now connected to MongoDB Atlas!"));

// Allows all resources to access our backend application
app.use(bodyParser.json());
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended:true }));

// Defines the "/users" string to be included for all user routes defined in the "userRoutes" file
app.use("/users", userRoutes);
app.use("/products", productRoutes);

// {PORT}
app.listen(process.env.PORT || 4000,() => {
    console.log(`API is now online on port ${process.env.PORT || 4000}`);
});







