const Product = require("../models/Product");



// Create product (ADMIN ONLY)
module.exports.addProduct = (req, res) => {
  if (!req.user.isAdmin) {
    return res.status(401).json({ error: "Only admin can create a product" });
  }

  const { name, description, price } = req.body;
  let newProduct = new Product({
    name: name,
    description: description,
    price: price
  });

  newProduct
    .save()
    .then(product => {
      res.json({ success: true });
    })
    .catch(error => {
      res.status(500).json({ error: "Failed to create product" });
    });
};

// Retrieve all products
module.exports.getAllProducts = (req, res) => {
  Product.find()
    .then(products => {
      res.json(products);
    })
    .catch(error => {
      res.status(500).json({ error: "Failed to retrieve products" });
    });
};

// Retrieve single product (GET)
module.exports.getProductById = (req, res) => {
  const productId = req.params.id;

  Product.findById(productId)
    .then(product => {
      if (product) {
        res.json(product);
      } else {
        res.status(404).json({ error: "Product not found" });
      }
    })
    .catch(error => {
      res.status(500).json({ error: "Failed to retrieve product" });
    });
};



// Retrieve all active products
module.exports.getAllActiveProducts = (req, res) => {
  Product.find({ isActive: true })
    .then(products => {
      res.json(products);
    })
    .catch(error => {
      res.status(500).json({ error: "Failed to retrieve active products" });
    });
};


// Update Product information (ADMIN)
module.exports.updateProduct = (req, res) => {
  if (!req.user.isAdmin) {
    return res.status(401).json({ error: "Only admin can update a product" });
  }

  const productId = req.params.id;
  const { name, description, price } = req.body;

  Product.findByIdAndUpdate(
    productId,
    { name, description, price },
    { new: true }
  )
    .then(updatedProduct => {
      if (updatedProduct) {
        res.json({ success: true, product: updatedProduct });
      } else {
        res.status(404).json({ error: "Product not found" });
      }
    })
    .catch(error => {
      res.status(500).json({ error: "Failed to update a product" });
    });
};


// Activate a product (ADMIN)
module.exports.activateProduct = (req, res) => {
  if (!req.user.isAdmin) {
    return res.status(401).json({ error: "Only admin can activate a product" });
  }

  const productId = req.params.id;

  Product.findByIdAndUpdate(
    productId,
    { isActive: true },
    { new: true }
  )
    .then(updatedProduct => {
      if (updatedProduct) {
        res.json({ success: true, product: updatedProduct });
      } else {
        res.status(404).json({ error: "Product not found" });
      }
    })
    .catch(error => {
      res.status(500).json({ error: "Failed to activate a product" });
    });
};


// ARCHIVE a product (ADMIN)
module.exports.archiveProduct = (req, res) => {
  if (!req.user.isAdmin) {
    return res.status(401).json({ error: "Only admin can archive a product" });
  }

  const productId = req.params.id;

  Product.findByIdAndUpdate(
    productId,
    { isActive: false },
    { new: true }
  )
    .then(updatedProduct => {
      if (updatedProduct) {
        res.json({ success: true, product: updatedProduct });
      } else {
        res.status(404).json({ error: "Product not found" });
      }
    })
    .catch(error => {
      res.status(500).json({ error: "Failed to archive a product" });
    });
};






