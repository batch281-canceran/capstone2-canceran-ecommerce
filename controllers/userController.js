const User = require("../models/User");
const Product = require("../models/Product")
const bcrypt = require("bcrypt");
const auth = require("../auth");


module.exports.registerUser = async (reqBody) => {
  try {
    const { email, password, isAdmin } = reqBody;

    const hashedPassword = await bcrypt.hash(password, 10);

    const newUser = new User({
      email,
      password: hashedPassword,
      isAdmin: isAdmin || false, // Set isAdmin to true if provided, otherwise default to false
    });

    const user = await newUser.save();

    return true;
  } catch (error) {
    console.error("Error during user registration:", error);
    return false;
  }
};

// auth for non admin 
module.exports.logInnonAdminuser = (reqBody) => {
  return User.findOne({ email: reqBody.email })
    .then((user) => {
      if (user === null) {
        return { success: false, message: 'Authentication failed' };
      } else {
        const isPasswordCorrect = bcrypt.compareSync(reqBody.password, user.password);
        if (isPasswordCorrect) {
          if (!user.isAdmin) {
            const token = auth.createAccessToken(user, false);
            return { success: true, access_token: token };
          } else {
            return { success: false, message: 'User is an admin' };
          }
        } else {
          return { success: false, message: 'Authentication failed' };
        }
      }
    })
    .catch((error) => {
      console.error('Error during login:', error);
      throw error;
    });
};


// Set as an ADMIN
module.exports.setAdminUser = (userId) => {
  return User.findByIdAndUpdate(userId, { isAdmin: true })
    .then(() => {
      return true;
    })
    .catch((error) => {
      console.error("Error setting user as admin:", error);
      return false;
    });
};



// User Authentication (ADMIN USER)
module.exports.loginUser = (reqBody) => {
  return User.findOne({ email: reqBody.email })
    .then((user) => {
      if (user === null) {
        return false;
      } else {
        const isPasswordCorrect = bcrypt.compareSync(
          reqBody.password,
          user.password
        );
        if (isPasswordCorrect) {
          if (user.isAdmin) {
            return { access: auth.createAccessToken(user, true) };
          } else {
            return false;
          }
        } else {
          return false;
        }
      }
    })
    .catch((error) => {
      console.error("Error during login:", error);
      throw error;
    });
};

// Retrieve User Details
module.exports.getProfile = (userId) => {
  return User.findById(userId)
    .then((user) => {
      if (!user) {
        throw new Error("User not found");
      }
      return user;
    });
}; 


// Non-admin User Checkout (Create order)
module.exports.createOrder = async (req, res) => {
  try {
    const { productName, email, quantity } = req.body;

    // Find the corresponding product based on the product name
    const product = await Product.findOne({ name: productName });

    if (!product) {
      return res.status(404).json({ message: 'Product not found' });
    }

    const order = {
      productId: product._id,
      productName,
      quantity,
      totalAmount: product.price * quantity,
    };

    const user = await User.findOne({ email });
    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }

    // Save the order to the user's orderedProducts array
    user.orderedProducts.push(order);
    await user.save();

    // Update the product's userOrders array with the user's ID
    product.userOrders.push({ userId: user._id });
    await product.save();

    res.status(200).json({ message: 'Order created successfully', order });
  } catch (error) {
    res.status(500).json({ message: 'An error occurred', error: error.message });
  }
};